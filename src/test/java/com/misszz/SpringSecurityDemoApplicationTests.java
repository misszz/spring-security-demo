package com.misszz;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class SpringSecurityDemoApplicationTests {

    @Test
   public void contextLoads() {
        System.out.println(">>>>>>>>>>>>>>");
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = encoder.encode("123");
        System.out.println(password);
        System.out.println(encoder.matches("123", "$2a$10$iwh4usDwC3aEFSGTOE9oq.kW/z2dE7cz8XgWNJXC13Pi/FbP.xlFu"));
    }

}
