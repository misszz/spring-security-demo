package com.misszz.config;

import com.misszz.handle.MyAccessDeniedHandler;
import com.misszz.handle.MyAuthenticationFailureHandle;
import com.misszz.handle.MyAuthenticationSuccessHandle;
import com.misszz.service.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * 通过在类方法中对一些url配置相应的权限
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private MyAuthenticationSuccessHandle successHandle;
    @Autowired
    private MyAuthenticationFailureHandle failureHandle;
    @Autowired
    private MyAccessDeniedHandler deniedHandler;
    @Autowired
    private UserDetailServiceImpl userDetailService;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private PersistentTokenRepository tokenRepository;
    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PersistentTokenRepository getPersistentTokenRepository(){
        JdbcTokenRepositoryImpl repository = new JdbcTokenRepositoryImpl();
        repository.setDataSource(dataSource);   //引入数据源
        //自动建表 只有当第一次启动时需要执行    第二次启动会报错因为已经存在了
        //repository.setCreateTableOnStartup(true);
        return repository;
    }

    //自定义登录页
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //启动表单认证
        http.formLogin()
                .usernameParameter("username111")   //表单标签现在可以设置成name=user111
                .loginPage("/userLogin")      //自定义登录页
                .loginProcessingUrl("/login")       //当发现/login时认为是登录，必须和表单提交的地址是一样的，去知名userDetailServiceImpl
                .successForwardUrl("/toSuccess")    //登录成功后跳转页面 Post
                //.successHandler(successHandle)  //自定义登录成功后的处理器
                .failureForwardUrl("/toError");
                //.failureHandler(failureHandle);     //自定义登录失败后的处理器


        http.authorizeRequests() //认真请求 下面选择哪些认证哪些拦截
                //.antMatchers("/login.html", "/error.html", "/success.html").permitAll()   //匹配对应的请求   允许还是拒绝
                .antMatchers("/*.html").permitAll()   //匹配对应的请求   允许还是拒绝
                //允许静态资源(放行)
                .antMatchers("/js/**").permitAll()
                //.antMatchers("/images/**").permitAll()
                .antMatchers("/images/**").access("permitAll")  //使用access表达式
                .antMatchers("/css/**").permitAll()
                .antMatchers("/login1").permitAll()
                .antMatchers("/userLogin").permitAll()
                //.regexMatchers(HttpMethod.POST,"/demo").permitAll()
                //.mvcMatchers("/misszz/demo", "/demo").permitAll()
                //.antMatchers("/demo").hasAuthority("admin1")     //必须具备某个权限才能访问到
                //.antMatchers("/demo").hasAnyAuthority("admin1,admin")  //必须具备某个权限才能访问到
                //.antMatchers("/demo").hasIpAddress("127.0.0.1") //设置固定ip拦截
                //.antMatchers("/demo").access("hasIpAddress('127.0.0.1')") //通过access表达式也能达到目的
                //.antMatchers("/demo").hasAnyRole("abc")     //必须具备某个角色才能访问到对应资源(目前为abc角色)
                .anyRequest().authenticated();//除了上面的所有请求都需要认证
                //.anyRequest().access("@myServiceImpl.hasPromission(request,authentication)");//也可以根据我们自己的需求对请求进行自定义的处理

        //http.exceptionHandling().accessDeniedPage("/denied.html");  //对权限不够尽兴处理(也就是拒绝访问)
        //http.exceptionHandling().accessDeniedHandler(deniedHandler);  //对权限不够尽兴处理(也就是拒绝访问)  自定义处理器
        //关闭csrf防护
        //http.csrf().disable();

        //记住我
        http.rememberMe()
                .tokenValiditySeconds(60*60*24)   //设置过期时间  一天
                .userDetailsService(userDetailService)      //自定义登录逻辑
                .tokenRepository(tokenRepository);

        //注销
        http.logout()
                //.addLogoutHandler()     //自定义注销处理器
                .logoutUrl("/logout")   //填写该地址后springsecurity会自动帮你处理注销
                .logoutSuccessUrl("/login.html");
    }
}
