package com.misszz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 用户点击表单登录时会执行该方法
 *
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("执行了loadUserByUsername方法");
        //模拟用户登录过程
        //1.在数据库中查询是否存在用户 不存在抛出异常
        if (!"admin".equals(username)){
            throw new UsernameNotFoundException("用户不存在");
        }
        //2.验证账号密码是否正确
        String password = passwordEncoder.encode("123");
        //实际操作应该都是从数据库中获取，这里为了方便而定义的权限和角色,权限都是普通的字符串，多个权限时以逗号分隔，而角色的话都是以ROLE_开头,后面结尾的为实际的角色`abc`（以下面为例的）
        return new User(username,password,
                AuthorityUtils.commaSeparatedStringToAuthorityList("admin,nomal,ROLE_abc"));
    }
}
