package com.misszz.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

public interface MyService {
    public boolean hasPromission(HttpServletRequest request, Authentication authentication);
}
