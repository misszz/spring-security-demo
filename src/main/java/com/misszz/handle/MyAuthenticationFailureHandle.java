package com.misszz.handle;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/8/22 13:12
 **/
@Component
public class MyAuthenticationFailureHandle implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        String message = e.getMessage();
        System.out.println(">>>>>>>>异常信息>>>>>>>>>"+message);
            response.sendRedirect("/toError");
    }
}
