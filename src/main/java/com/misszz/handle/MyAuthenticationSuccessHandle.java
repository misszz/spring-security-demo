package com.misszz.handle;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wangzhongxu
 * @version 1.0
 * *@description
 * @since 2020/8/22 12:07
 **/
@Component
public class MyAuthenticationSuccessHandle implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        System.out.println("-------------------------用户登录成功-------------------------------");
        String addr = request.getRemoteAddr();      //获取登录成功设备的ip地址
        System.out.println("addr = " + addr);
        User user = (User)authentication.getPrincipal();
        System.out.println(user.getUsername());
        System.out.println(user.getPassword());
        user.getAuthorities().forEach(grantedAuthority -> {
            String authority = grantedAuthority.getAuthority();
            System.out.println("authority = " + authority);
        });
        response.sendRedirect("/toSuccess");
        System.out.println("-------------------------用户登录成功-------------------------------");
    }
}
