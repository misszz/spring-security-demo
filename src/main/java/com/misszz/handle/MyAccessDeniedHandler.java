package com.misszz.handle;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 使用的缘由：经常看到403那种无权限访问的状态码，对用户很不友好
 * 一般我都都会采取方式自定义页面
 */
@Component
public class MyAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
        System.out.println(">>>>>>>>>>>>>>MyAccessDeniedHandler>>>>>>>>>>>>>>>>");
        System.out.println(e.getMessage());
        response.sendRedirect("/denied.html");
        System.out.println(">>>>>>>>>>>>>>MyAccessDeniedHandler>>>>>>>>>>>>>>>>");
    }
}