package com.misszz.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
    @RequestMapping("/userLogin")
    public String login(){
        System.out.println("执行了login方法");
        return "login";
    }
    //@Secured("abC")    //只有具备某个角色时才能访问,前缀必须是ROLE_,不然会报错
    @PreAuthorize("hasRole('ROLE_abc')") //在方法访问或类在执行之前先判断权限，大多数都是使用该注解，注解里面的参数和access()方法的取值相同，都是权限表达式,该注解中ROLE_开头可加可不加
    //@PostAuthorize()  //在方法访问或类在执行结束后在执行该方法，其注解很少使用你
    @RequestMapping("/toSuccess")
    public String toSuccess(){
        System.out.println("执行了toSuccess方法");
        return "redirect:/success.html";
    }
    @RequestMapping("/toError")
    public String toError(){
        System.out.println("执行了toError方法");
        return "redirect:/error.html";
    }
    @RequestMapping("/show")
    public String show(){
        System.out.println("执行了show方法");
        return "redirect:/show.html";
    }

    @RequestMapping("/login1")
    public String login1(){
        return "redirect:/login1.html";
    }

    @RequestMapping("/demo001")
    public String demo001(){
        return "demo";
    }
}
