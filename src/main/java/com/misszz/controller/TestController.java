package com.misszz.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @RequestMapping("/test")
    public String test(){
        return "test";
    }

    //@Secured("abc")    //只有具备某个角色时才能访问,前缀必须是ROLE_,不然会报错
    @PreAuthorize("hasRole('abc')")
    @GetMapping("/demo")
    public void demo(){
        System.out.println("执行了demo方法");
        System.out.println("...............");
    }
}
